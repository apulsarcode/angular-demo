import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.css']
})

export class BestPassComponent {
    formGroup: FormGroup;
    from: Date;
    to: Date;
    dateFormat: string = '^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}$';
    apiUrl: string     = 'https://loc.api.parclick.com/v1/pass/';
    pass: Pass;
    errors: any;
    searching: boolean = false;

    constructor(private formBuilder: FormBuilder, private http: Http) {
        this.formGroup = formBuilder.group({
            from: [null, Validators.compose([Validators.required, Validators.pattern(this.dateFormat)])],
            to  : [null, Validators.compose([Validators.required, Validators.pattern(this.dateFormat)])]
        });
    }

    submit(data) {
        this.from = data.from;
        this.to   = data.to;
        this.formGroup.disable();
        this.search();
    }

    search() {
        this.pass = null;
        this.errors = null;
        this.searching = true;
        let url = this.apiUrl + '?locale=en_GB&group=bestpass&parking=263&from=' + this.from + '&to=' + this.to + '&vehicleType=1';
        this.http.get(url).subscribe(
            response => {
                this.formGroup.enable();
                this.pass = response.json().items[0];
                this.searching = false;
            },
            error => {
                this.formGroup.enable();
                this.searching = false;
                let errors = [];

                for (let children in error.json().errors.children) {
                    if (error.json().errors.children[children]['errors']) {
                        errors.push(children + ': ' + error.json().errors.children[children]['errors'][0]);
                    }
                }

                this.errors = errors;
            }
        );
    }
}

interface Pass {
    id: number;
    name: string;
    price: number;
    duration: number;
}

interface FormErrors {
    code: number;
    errors: object;
    message: string;
}
