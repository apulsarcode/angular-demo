import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BestPassComponent } from './app.component';

@NgModule({
    declarations: [
        BestPassComponent
    ],
    imports     : [
        BrowserModule,
        HttpModule,
        ReactiveFormsModule
    ],
    providers   : [],
    bootstrap   : [BestPassComponent]
})
export class AppModule {
}
